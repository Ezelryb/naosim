\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Planung}{4}% 
\contentsline {subsection}{\numberline {1.1}Problembaum}{4}% 
\contentsline {subsection}{\numberline {1.2}Zielbaum}{5}% 
\contentsline {section}{\numberline {2}Analyse}{6}% 
\contentsline {subsection}{\numberline {2.1}Funktionsbaum}{16}% 
\contentsline {subsection}{\numberline {2.2}Anwendungsf\IeC {\"a}lle}{17}% 
\contentsline {subsection}{\numberline {2.3}Aktivit\IeC {\"a}ten}{18}% 
\contentsline {subsection}{\numberline {2.4}Pakete}{21}% 
\contentsline {subsection}{\numberline {2.5}Klassen}{22}% 
\contentsline {section}{\numberline {3}Entwurf}{23}% 
\contentsline {subsection}{\numberline {3.1}Pakete}{23}% 
\contentsline {subsection}{\numberline {3.2}Klassen}{24}% 
\contentsline {subsection}{\numberline {3.3}Klassenvertr\IeC {\"a}ge}{25}% 
\contentsline {subsection}{\numberline {3.4}Abnahmekriterien}{27}% 
