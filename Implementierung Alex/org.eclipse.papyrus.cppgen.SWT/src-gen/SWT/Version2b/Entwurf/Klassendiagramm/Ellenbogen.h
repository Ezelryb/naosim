// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_ELLENBOGEN_H
#define SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_ELLENBOGEN_H

/************************************************************
 Ellenbogen class header
 ************************************************************/

//Pkg_Klassendiagramm.h"

#include "../../../../PrimitiveTypes/Pkg_PrimitiveTypes.h"
#include "Koerperteil.h"
#include "ode/ode.h"

// Include from Include stereotype (header)
#pragma once
#include <iostream>
#include "ode/ode.h"
#include "drawstuff/drawstuff.h"
// End of Include stereotype (header)

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

/************************************************************/
/**
 * 
 */
class Ellenbogen: public Koerperteil {
public:

	/**
	 * 
	 * @param raum 
	 * @param groesse 
	 */
	void setGroesse(dSpaceID& /*in*/raum,
			::PrimitiveTypes::Real* /*in*/groesse);

	/**
	 * 
	 * @param masse 
	 */
	void setMasse(::PrimitiveTypes::Real /*in*/masse);

	/**
	 * 
	 * @return  
	 */
	::PrimitiveTypes::Real* getGroesse();

	/**
	 * 
	 * @param welt 
	 */
	Ellenbogen(dWorldID& /*in*/welt);

	/**
	 * 
	 */
	~Ellenbogen();

	/**
	 * 
	 */
	void zeichne();

private:

	/**
	 * 
	 */
	::PrimitiveTypes::Real radius;
};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Ellenbogen class header
 ************************************************************/

#endif
