// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#define SWT_Version2b_Entwurf_Klassendiagramm_Koerperteilfabrik_BODY

/************************************************************
 Koerperteilfabrik class body
 ************************************************************/

// include associated header file
#include "Koerperteilfabrik.h"

// Derived includes directives
#include "Koerperteil.h"

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

// static attributes (if any)

/**
 * 
 */
Koerperteilfabrik::~Koerperteilfabrik() {
}

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Koerperteilfabrik class body
 ************************************************************/
