#ifndef PKG_SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM
#define PKG_SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM

/************************************************************
 Pkg_Klassendiagramm package header
 ************************************************************/

#include "SWT/Version2b/Entwurf/Pkg_Entwurf.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

// Types defined within the package
}// of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Pkg_Klassendiagramm package header
 ************************************************************/

#endif
