// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#define SWT_Version2b_Entwurf_Klassendiagramm_Koerperteil_BODY

/************************************************************
 Koerperteil class body
 ************************************************************/

// include associated header file
#include "Koerperteil.h"

// Derived includes directives

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

// static attributes (if any)

/**
 * 
 * @return  
 */
dGeomID Koerperteil::getGeomID() {
	return this->geom;
}

/**
 * 
 * @return  
 */
dBodyID Koerperteil::getKoerperID() {
	return this->id;
}

/**
 * 
 * @return position 
 */
const ::PrimitiveTypes::Real* Koerperteil::getPosition() {
	return dBodyGetPosition(this->id);
}

/**
 * 
 * @return rotation 
 */
const ::PrimitiveTypes::Real* Koerperteil::getRotation() {
	return dBodyGetRotation(this->id);
}

/**
 * 
 * @param x 
 * @param y 
 * @param z 
 */
void Koerperteil::setPosition(::PrimitiveTypes::Real /*in*/x,
		::PrimitiveTypes::Real /*in*/y, ::PrimitiveTypes::Real /*in*/z) {
	dBodySetPosition(this->id, x, y, z);
}

/**
 * 
 * @param x 
 * @param y 
 * @param z 
 */
void Koerperteil::setRotation(::PrimitiveTypes::Real /*in*/x,
		::PrimitiveTypes::Real /*in*/y, ::PrimitiveTypes::Real /*in*/z) {
	dMatrix3 r;
	dRFromEulerAngles(r, x, y, z);
	dBodySetRotation(this->id, r);
}

/**
 * 
 * @param welt 
 */
Koerperteil::Koerperteil(dWorldID& /*in*/welt) {
	this->id = dBodyCreate(welt);
}

/**
 * 
 */
Koerperteil::~Koerperteil() {
	dBodyDestroy(this->id);
}

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Koerperteil class body
 ************************************************************/
