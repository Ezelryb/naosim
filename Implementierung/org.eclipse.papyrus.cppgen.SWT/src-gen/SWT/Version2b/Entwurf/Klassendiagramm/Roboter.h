// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_ROBOTER_H
#define SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_ROBOTER_H

/************************************************************
 Roboter class header
 ************************************************************/

//Pkg_Klassendiagramm.h"

#include "../../../../PrimitiveTypes/Pkg_PrimitiveTypes.h"
#include "ode/ode.h"

// Include from Include stereotype (header)
#pragma once
#include <iostream>
#include "Scharniergelenk.h"
#include "Koerperteilfabrik.h"
// End of Include stereotype (header)

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {
class Gelenk;
}
}
}
}
namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {
class Koerperteil;
}
}
}
}

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

/************************************************************/
/**
 * 
 */
class Roboter {
public:

	/**
	 * 
	 * @param welt 
	 * @param raum 
	 */
	void erzeugeRoboter(dWorldID& /*in*/welt, dSpaceID& /*in*/raum);

	/**
	 * 
	 */
	void zeichneRoboter();

	/**
	 * 
	 * @param welt 
	 */
	void erzeugeGelenke(dWorldID& /*in*/welt);

	/**
	 * 
	 */
	void updateWinkel();

	/**
	 * 
	 * @param delta_theta 
	 */
	void aendereLWristYaw(::PrimitiveTypes::Real /*in*/delta_theta);

	/**
	 * 
	 * @param delta_theta 
	 */
	void aendereRWristYaw(::PrimitiveTypes::Real /*in*/delta_theta);

	/**
	 * 
	 */
	void oeffneLHand();

	/**
	 * 
	 */
	void schliesseLHand();

	/**
	 * 
	 */
	void oeffneRHand();

	/**
	 * 
	 */
	void schliesseRHand();

	/**
	 * 
	 */
	void aendereLElbowRoll(::PrimitiveTypes::Real /*in*/delta_theta);

	/**
	 * 
	 */
	void aendereRElbowRoll(::PrimitiveTypes::Real /*in*/delta_theta);

private:

	/**
	 * 
	 */
	Koerperteil* LHand;

	/**
	 * 
	 */
	Koerperteil* LBizeps;

	/**
	 * 
	 */
	Koerperteil* LVorderarm;


	/**
	 * 
	 */
	Koerperteil* RHand;

	/**
	 * 
	 */
	Koerperteil* RBizeps;

	/**
	 * 
	 */
	Koerperteil* RVorderarm;
	
	/**
	 * 
	 */
	Gelenk* LWristYaw;

	/**
	 * 
	 */
	Gelenk* RWristYaw;

	/**
	 * 
	 */
	Gelenk* RElbowRoll;

	/**
	 * 
	 */
	Gelenk* LElbowRoll;

	/**
	 * 
	 */
	Koerperteil* LFinger[3];

	/**
	 * 
	 */
	Koerperteil* RFinger[3];

	/**
	 * 
	 */
	Gelenk* LFingerGelenk[3];

	/**
	 * 
	 */
	Gelenk* RFingerGelenk[3];
};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Roboter class header
 ************************************************************/

#endif
