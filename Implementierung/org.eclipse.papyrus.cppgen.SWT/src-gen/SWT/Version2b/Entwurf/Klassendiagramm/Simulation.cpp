// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#define SWT_Version2b_Entwurf_Klassendiagramm_Simulation_BODY

/************************************************************
 Simulation class body
 ************************************************************/

// include associated header file
#include "Simulation.h"

// Derived includes directives
#include "Roboter.h"

// Include from Include declaration (body)
using namespace SWT::Version2b::Entwurf::Klassendiagramm;

int main(int argc, char* argv[]) {
	Simulation sim;
	sim.starteSimulation(argc, argv);
}

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {

// static attributes (if any)
/**
 * 
 */
dWorldID Simulation::welt;
/**
 * 
 */
dSpaceID Simulation::raum;
/**
 * 
 */
dGeomID Simulation::boden;
/**
 * 
 */
Roboter* Simulation::nao;
/**
 * 
 */
dJointGroupID Simulation::contactgroup;
/**
 * 
 */
::PrimitiveTypes::Integer Simulation::flag;

/**
 * 
 * @param argc 
 * @param argv 
 */
void Simulation::starteSimulation(::PrimitiveTypes::Integer& /*in*/argc,
		char** /*in*/argv) {
	this->nao = new Roboter;
	dsFunctions fn;
	fn.version = DS_VERSION;
	fn.start = &Simulation::start;
	fn.step = &Simulation::simLoop;
	fn.command = &Simulation::command;
	fn.stop = NULL;
	fn.path_to_textures = "/home/skranz/Downloads/ode-0.16/drawstuff/textures";

	dInitODE();
	Simulation::welt = dWorldCreate();
	Simulation::raum = dHashSpaceCreate(0);

	std::cout << "create:" << "\t" << Simulation::raum << std::endl;
	Simulation::contactgroup = dJointGroupCreate(0);
	Simulation::boden = dCreatePlane(Simulation::raum, 0, 0, 1, 0);

	this->nao->erzeugeRoboter(Simulation::welt, Simulation::raum);
	this->nao->erzeugeGelenke(Simulation::welt);

	dWorldSetERP(Simulation::welt, 0.8);
	dWorldSetCFM(Simulation::welt, 1E-3);

	dsSimulationLoop(argc, argv, 1600, 900, &fn);

	dWorldDestroy(Simulation::welt);
	dCloseODE();
}


/**
 * 
 * @param pause 
 */
void Simulation::simLoop(::PrimitiveTypes::Integer /*in*/pause) {
	Simulation::flag = 0;

	if (!pause) {
		nao->updateWinkel();
		dSpaceCollide(Simulation::raum, 0, &nearCallback);
		dWorldStep(Simulation::welt, 0.02);
		dJointGroupEmpty(Simulation::contactgroup);
	}

	if (flag == 0)
		dsSetColor(1.0, 0.0, 1.0);
	else
		dsSetColor(0.0, 1.0, 0.0);
	nao->zeichneRoboter();
}

/**
 * 
 */
void Simulation::start() {
	static float xyz[3] = { 0.0, -3.0, 1.0 };
	static float hpr[3] = { 90.0, 0.0, 0.0 };

	dsSetViewpoint(xyz, hpr);
}

/**
 * 
 * @param data 
 * @param o1 
 * @param o2 
 */
void Simulation::nearCallback(void* /*in*/data, dGeomID /*in*/o1,
		dGeomID /*in*/o2) {
	const int N = 10;
	dContact contact[N];

	int isGround = ((Simulation::boden == o1) || (Simulation::boden == o2));

	int n = dCollide(o1, o2, N, &contact[0].geom, sizeof(dContact));

	if (isGround) {
		if (n >= 1)
			Simulation::flag = 1;
		else
			Simulation::flag = 0;
		for (int i = 0; i < n; i++) {
			contact[i].surface.mode = dContactBounce;
			contact[i].surface.mu = dInfinity;
			contact[i].surface.bounce = 0.0;
			dJointID c = dJointCreateContact(Simulation::welt,
					Simulation::contactgroup, &contact[i]);
			dJointAttach(c, dGeomGetBody(contact[i].geom.g1),
					dGeomGetBody(contact[i].geom.g2));
		}
	}
}

/**
 * 
 * @param cmd 
 */
void Simulation::command(::PrimitiveTypes::Integer /*in*/cmd) {
	double delta_theta = 0.05;

	switch (cmd) {
	case 'r':
		nao->aendereLElbowRoll(delta_theta);
		break;
	case 't':
		nao->aendereLElbowRoll(-delta_theta);
		break;
	case 'f':
		nao->aendereLWristYaw(delta_theta);
		break;
	case 'g':
		nao->aendereLWristYaw(-delta_theta);
		break;
	case 'v':
		nao->oeffneLHand();
		break;
	case 'b':
		nao->schliesseLHand();
		break;
	case 'o':
		nao->aendereRElbowRoll(delta_theta);
		break;
	case 'i':
		nao->aendereRElbowRoll(-delta_theta);
		break;
	case 'k':
		nao->aendereRWristYaw(delta_theta);
		break;
	case 'j':
		nao->aendereRWristYaw(-delta_theta);
		break;
	case 'm':
		nao->oeffneRHand();
		break;
	case 'n':
		nao->schliesseRHand();
		break;
	}
}

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Simulation class body
 ************************************************************/
