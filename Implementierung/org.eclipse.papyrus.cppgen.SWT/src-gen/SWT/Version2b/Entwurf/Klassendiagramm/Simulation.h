// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_SIMULATION_H
#define SWT_VERSION2B_ENTWURF_KLASSENDIAGRAMM_SIMULATION_H

/************************************************************
 Simulation class header
 ************************************************************/

//Pkg_Klassendiagramm.h"


#include "../../../../PrimitiveTypes/Pkg_PrimitiveTypes.h"
#include "ode/ode.h"

// Include from Include stereotype (header)
#pragma once
#include <iostream>
#include "ode/ode.h"
#include "drawstuff/drawstuff.h"
#include "Koerperteil.h"

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {
class Roboter;
}
}
}
}

namespace SWT {
namespace Version2b {
namespace Entwurf {
namespace Klassendiagramm {


class Simulation {
public:

	/**
	 * 
	 * @param argc 
	 * @param argv 
	 */
	void starteSimulation(::PrimitiveTypes::Integer& /*in*/argc,
			char** /*in*/argv);

	/**
	 * 
	 * @param pause 
	 */
	static void simLoop(::PrimitiveTypes::Integer /*in*/pause);

	/**
	 * 
	 */
	static void start();

	/**
	 * 
	 * @param data 
	 * @param o1 
	 * @param o2 
	 */
	static void nearCallback(void* /*in*/data, dGeomID /*in*/o1,
			dGeomID /*in*/o2);

	/**
	 * 
	 * @param cmd 
	 */
	static void command(::PrimitiveTypes::Integer /*in*/cmd);

private:

	/**
	 * 
	 */
	static dWorldID welt;

	/**
	 * 
	 */
	static dSpaceID raum;

	/**
	 * 
	 */
	static dGeomID boden;

	/**
	 * 
	 */
	static Roboter* nao;

	/**
	 * 
	 */
	static dJointGroupID contactgroup;

	/**
	 * 
	 */
	static ::PrimitiveTypes::Integer flag;
};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace Klassendiagramm
} // of namespace Entwurf
} // of namespace Version2b
} // of namespace SWT

/************************************************************
 End of Simulation class header
 ************************************************************/

#endif
