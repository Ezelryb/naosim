# NaoSim

## Dokumente
 - *LH* Lastenheft
 - *PH* Pflichtenheft
 - *UseCase* Anwendungsfalldiagramm
 - *Activity* Aktivitätendiagramm
 - *Targets* Zielbaum
 - *Problems* Problembaum
 - *Functions* Funktionsbaum
 - *PackA* Paketdiagramm der Analyse
 - *PackE* Paketdiagramm des Entwurfs
 - *ClassA* Klassendiagramm der Analyse
 - *ClassE* Klassendiagramm des Entwurfs
 - *Install* Installationsanleitung
 
## digitale Abgabe
 - Papyrus Projekt (`.di` Datei)
 - Implementierung
 - Hausarbeit in PDF

## Links
 - In die C++ Version von Eclipse das [Papyrus Plug-in](https://download.eclipse.org/modeling/mdt/papyrus/components/designer/) installieren
	- [Tutorial](https://wiki.eclipse.org/Papyrus/Codegen/CppHelloWorld) um Code zu generieren
 - [ODE Wiki](https://www.ode-wiki.org/wiki)
 - [Drawstuff Tutorial](http://demura.net/english)
 - Nao Dokumentation
	- [http://doc.aldebaran.com/2-1/naoqi/index.html](http://doc.aldebaran.com/2-1/naoqi/index.html)
	- [http://doc.aldebaran.com/2-1/family/robots/masses_robot.html](http://doc.aldebaran.com/2-1/family/robots/masses_robot.html)
