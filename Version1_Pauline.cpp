#include <iostream>
#include <string>
#include <ode/ode.h> 
#include <drawstuff/drawstuff.h>
#define Anz 3 //Anzahl der Koerperteile/Gelenke, wobei 0 sich nicht bewegt
using namespace std;

static string name[Anz] = {"LeftFoot", "LeftAnkle", "LeftTibia"}; //Namen der Koerperteile
static double hoehe[Anz] = {0.30, 0.05, 1.00}; //Hoehe der Koerperteile
static double radius[Anz] = {0.60, 0.05, 0.07}; //Umfang der Koerperteile
dReal Fussgroesse[3] = {0.4, 0.8, 0.2};
static string nameG[Anz] = {" ", "LAnkleRoll", "LAnklePitch"}; //Name der Gelenke
static double status[Anz] = {0.0, 0.0, 0.0}; //Aktueller Winkelstand der Gelenke
static double maximum[Anz] = {0.0, 0.769001, 0.922747}; //Maximaler Winkel
static double minimum[Anz] = {0.0, -0.397880, -1.189516}; //Minimaler Winkel
dWorldID Welt; //Dynamische Welt 
dBodyID Teil[Anz]; //Koerperteile, Teil[0] ist der Fuss
dJointID Gelenk[Anz]; //Gelenke, wobei Gelenk[0] fixiert ist
dsFunctions fn; //Variable fuer Drawstuff

class Bewegung {
	public:
	//Methoden
	void bewege(string name, double wert);
	void informiere();
	void erstelleGelenk();
};

void bewege(int pos, double wert) {
	//Gelenk und zu Veraenderung des Winkels werden uebergeben
	int p = pos;
	double w = wert;
	status[p] += w;

	//Ueberpruefen, ob maximaler oder minimaler Wert ueberschritten wird.
	if(status[1] < minimum[1]) 
		status[1] = minimum[1];
	if(status[1] > maximum[1])
		status[1] = maximum[1];
	if(status[2] < minimum[2])
		status[2] = minimum[2];
	if(status[2] > maximum[2])
		status[2] = maximum[2];
}

void informiere() {
	//Winkelinformation zu jedem Gelenk wird ausgegeben
	for (int i = 1; i < Anz; i++){
		std::cout<<"Gelenk: "<<nameG[i]<<" , aktueller Winkel: "<<status[i]<<endl;
	}
}

void erstelleGelenk() {
double anchor_x[Anz]  = {0.00}, anchor_y[Anz] = {0.00}; //Ankerpunkt; 
	double anchor_z[Anz] = { 0.00, 0.10, 0.15};
	double axis_x[Anz]  = { 0.00, 0.00, 0.00};  //Rotationsachse; 
	double axis_y[Anz]  = { 0.00, 0.00, 1.00};
	double axis_z[Anz]  = { 1.00, 1.00, 0.00};
	
	Gelenk[0] = dJointCreateFixed(Welt, 0); //Fuss ist am Boden fixiert; 
	dJointAttach(Gelenk[0], Teil[0], 0);      
	dJointSetFixed(Gelenk[0]);   
	for (int j = 1; j <Anz; j++) {
        	Gelenk[j] = dJointCreateHinge(Welt, 0);     // create a hinge joint; 
        	dJointAttach(Gelenk[j], Teil[j-1], Teil[j]); // attach the joints; 
        	dJointSetHingeAnchor(Gelenk[j], anchor_x[j], anchor_y[j],anchor_z[j]); // set an anchor point; 
		dJointSetHingeAxis(Gelenk[j], axis_x[j], axis_y[j], axis_z[j]); // set an rotation axis; 
	}   
}
class KoerperteilFabrik {
	public:
	void erstelle();
};

void erstelle() {
	double x[Anz] = {0.00, 0.00, 0.00};
	double y[Anz] = {0.20, 0.00, 0.00};  
	double z[Anz] = { 0.00, 0.10, 0.60};
	double m[Anz] = {0.17184, 0.13416, 0.30142};           //Masse des Roboterfusses;
	for (int i = 0; i <Anz; i++) {     // creation and setting of links; 
		dMass mass;  // mass parameter; 
        	Teil[i] = dBodyCreate(Welt);     // create link; 
        	dBodySetPosition(Teil[i], x[i], y[i], z[i]); // set position; 
        	dMassSetZero(&mass);      // initialize the mass parameter; 
        	dMassSetCapsuleTotal(&mass,m[i],3,radius[i],hoehe[i]);  // calculate the mass parameter; 
        	dBodySetMass(Teil[i], &mass);  // set the mass parameter to the body; 
	}
}

class Koerperteil {
	public:
	void zeichne();
};

void zeichne() {
	dsSetColor(1.0,1.0,1.0); // Farbe; 
    	/*for (int i = 0; i <Anz; i++ ) 
        	dsDrawCapsule(dBodyGetPosition(Teil[i]), dBodyGetRotation(Teil[i]), hoehe[i], radius[i]);*/
	//Fuss
	dsDrawBox(dBodyGetPosition(Teil[0]), dBodyGetRotation(Teil[0]), Fussgroesse);
	//Knoechel
	dsSetSphereQuality(3);
	dsDrawSphere(dBodyGetPosition(Teil[1]), dBodyGetRotation(Teil[1]), 0.16);
	//Schienenbein
	dsDrawCapsule(dBodyGetPosition(Teil[2]), dBodyGetRotation(Teil[2]), 1.0, 0.15);

}


class Welt {
	public:
	void start();
	void kontrolliere();
	void eingabe(int ein);
	void simLoop(int pause);
};

void start() { // Umgebung
	float punkt[3] = {  3.04, 1.28, 0.76};   // Ausgangspunkt der Simulation; x, y, z　[m]
	float richtung[3] = { -160.0, 4.50, 1.00};  // Richtung in die man sieht　[°]
	dsSetViewpoint(punkt,richtung);               // Festlegen des Punktes und der Richtung
}

void kontrolliere() {
	static int schritt = 0;     
	double k1 =  10.0,  fMax  = 100.0; // k1 Anstieg, fMax: maximaler Wert
	printf("\r%6d:",schritt++);
	for (int j = 1; j <Anz; j++) {
        	double tmpAngle = dJointGetHingeAngle(Gelenk[j]);  // aktuelles Koerperteil
        	double z = status[j] - tmpAngle;  //z = aktueller Winkelwert - Koerperteil
       		dJointSetHingeParam(Gelenk[j],  dParamVel,  k1*z);  
        	dJointSetHingeParam(Gelenk[j], dParamFMax, fMax);  
    	}
}

void eingabe(int ein) { //Nutzereingabe
	switch (ein) {
		case 'j': //Winkelverkleinerung von LAnkleRoll
			bewege(1, -0.05);
			break;
		case 'J': //Winkelvergroesserung von LAnkleRoll
			bewege(1, 0.05);
			break;
		case 'k': //Winkelverkleinerung von LAnklePitch
			bewege(2, -0.05);
			break;
		case 'K': //Winkelvergroesserung von LAnklePitch
			bewege(2, 0.05);
			break;
		case 'I': //Winkelinformationen
			informiere();
			break;
	}
}
	
void simLoop(int pause) { //Simulation Loop
	kontrolliere();
    	dWorldStep(Welt, 0.02);
  	// Roboter zeichnen 
	zeichne();
}

class WeltDrawstuff {
public:
void setDrawstuff();
};

void setDrawstuff() {
	fn.version = DS_VERSION;
	fn.start   = &start;
	fn.step    = &simLoop;
	fn.command = &eingabe;
	fn.path_to_textures = "textures";
}

int main(int argc, char *argv[]) {
	
	setDrawstuff();
	

	dInitODE();  //Initialisiere ODE; 
	Welt = dWorldCreate();  //Erstellen einer dynamischen Welt;
	dWorldSetGravity(Welt, 0, 0, 0); //Gravitation;

	erstelle();
	erstelleGelenk();
	        
	dsSimulationLoop(argc, argv, 640, 570, &fn); // simulation loop;
	dCloseODE(); //schliesse ODE
	return 0;
}

